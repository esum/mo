#!/usr/bin/python3

import argparse
import os
import re
import stat
import sys


def check_sources(sources):
	for source in sources:
		try:
			os.stat(source, follow_symlinks=False)
			continue
		except FileNotFoundError:
			print(sys.argv[0], ': cannot stat ', repr(source), ': No such file or directory', file=sys.stderr, sep='')
		except PermissionError:
			print(sys.argv[0], ': cannot stat ', repr(source), ': Permission denied', file=sys.stderr, sep='')
		except Exception as exn:
			print(sys.argv[0], ': cannot stat ', repr(source), ': ', repr(exn), file=sys.stderr, sep='')
			raise exn
		exit(1)

def check_dest(dest, source=None):
	try:
		mode = os.stat(dest).st_mode
		if stat.S_ISDIR(mode) and source is not None:
			if (os.path.realpath(source)+'/').startswith(os.path.realpath(dest)+'/'):
				print(sys.argv[0], ': cannot move ', repr(source), ' to a subdirectory of itself', repr(dest), file=sys.stderr, sep='')
				exit(1)
		return mode
	except FileNotFoundError:
		return 0
	except PermissionError:
		print(sys.argv[0], ': cannot stat ', repr(source), ': Permission denied', file=sys.stderr, sep='')
		exit(1)
	except Exception as exn:
		print(sys.argv[0], ': cannot stat ', repr(source), ': ', repr(exn), file=sys.stderr, sep='')
		raise exn
		exit(1)


def ask_overwrite(dest):
	return input(sys.argv[0] + ': overwrite ' + repr(dest) + '? ').lower() == 'y'

def ask_override(dest, mode):
	return input(sys.argv[0] + ': replace ' + repr(dest) + ' , overriding mode {0:04o}? '.format(mode & 0o7777)).lower() == 'y'


def move(source, dest, mode=None, force=False, interactive=False, no_clobber=False, symbolic=False, verbose=False, dry=False):
	if mode is None:
		mode = os.stat(dest).st_mode
	if no_clobber and mode:
		return
	elif interactive and mode:
		if ask_overwrite(dest):
			if not dry: os.replace(source, dest)
		else:
			return
	elif force or stat.S_IWUSR & mode:
		if not dry: os.replace(source, dest)
	elif mode:
		if ask_override(dest, mode):
			if not dry: os.replace(source, dest)
		else:
			return
	elif not dry:
		if verbose: print('renamed', repr(source), '->', repr(dest), file=sys.stderr)
		os.rename(source, dest)
	if symbolic and not dry:
		os.symlink(dest, source)


def main(sources, dest, force=False, interactive=False, no_clobber=False, parents=False, symbolic=False, regex=None, verbose=False, dry=False):
	check_sources(sources)
	if regex is None:
		mode = check_dest(dest)
		if stat.S_ISDIR(mode):
			dests = [os.path.sep.join([dest, source.split(os.path.sep)[-1]]) for source in sources]
			modes = []
			for source, dest in zip(sources, dests):
				mode = check_dest(dest, source)
				if stat.S_ISDIR(mode):
					print(sys.argv[0], ': cannot overwrite directory ', repr(dest),' with non-directory', file=sys.stderr, sep='')
					exit(1)
				modes.append(mode)
			for source, dest, mode in zip(sources, dests, modes):
				move(source, dest, mode, force, interactive, no_clobber, symbolic, verbose, dry)
		elif len(sources) > 1:
			print(sys.argv[0], ': target ', repr(dest),' is not a directory', file=sys.stderr, sep='')
		else:
			source = sources[0]
			move(source, dest, mode, force, interactive, no_clobber, symbolic, verbose, dry)
	else:
		dests = []
		modes = []
		for source in sources:
			current_dest, valid = regex.subn(dest, source, count=1)
			if valid:
				dests.append(current_dest)
				modes.append(check_dest(current_dest, source))
				if verbose: print('will rename', repr(source), '->', repr(current_dest), file=sys.stderr)
			else:
				dests.append(None)
				modes.append(0)
		if len(set(dests)) != len(dests) - ((dests.count(None) - 1) if None in dests else 0):
			print(sys.argv[0], ': name confict: ', dests, file=sys.stderr, sep='')
			exit(1)
		for source, dest, mode in zip(sources, dests, modes):
			if dest is None:
				continue
			move(source, dest, mode, force, interactive, no_clobber, symbolic, verbose, dry)


if __name__ == '__main__':
	parser = argparse.ArgumentParser(prog='mo')
	parser.add_argument('-f', '--force', help='do not prompt before overwriting', action='store_true')
	parser.add_argument('-i', '--interactive', help='prompt before overwrite', action='store_true')
	parser.add_argument('-n', '--no-clobber', help='do not overwrite an existing file', action='store_true')
	parser.add_argument('-v', '--verbose', help='explain what is being done', action='store_true')
	parser.add_argument('-d', '--dry', help='do not make any change', action='store_true')
	#parser.add_argument('-p', '--parents', help='make parent directories as needed', action='store_true')
	parser.add_argument('-s', '--symbolic', help='make symnbolic link from SOURCE to DEST', action='store_true')
	parser.add_argument('-R', '--regex', help='move SOURCE matching REGEX to DEST', default=None, type=lambda r:re.compile('^'+r+'$'))
	parser.add_argument('sources', metavar='SOURCE', nargs='+')
	parser.add_argument('dest', metavar='DEST')
	#parser.add_argument('--help', 'display this help and exit')
	args = parser.parse_args()
	main(args.sources, args.dest, args.force, args.interactive, args.no_clobber, False, args.symbolic, args.regex, args.verbose, args.dry)
